INSERT INTO sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
  VALUES ('master', 'node', 'W');
INSERT INTO sym_channel (channel_id, processing_order, max_batch_size, enabled, description)
  VALUES ('fcyt_transaction', 1, 100000, 1, 'fcyt transaction');
INSERT INTO sym_trigger (trigger_id, source_table_name, channel_id, last_update_time, create_time)
  VALUES ('master_outbound', '*', 'fcyt_transaction', current_timestamp, current_timestamp);
INSERT INTO sym_router (router_id, source_node_group_id, target_node_group_id, router_type, create_time, last_update_time)
  VALUES ('master_2_node', 'master', 'node', 'default', current_timestamp, current_timestamp);
INSERT INTO sym_trigger_router (trigger_id, router_id, initial_load_order, last_update_time, create_time)
  VALUES ('master_outbound', 'master_2_node', 100, current_timestamp, current_timestamp);