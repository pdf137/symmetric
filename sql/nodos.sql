DELETE FROM confcyt.sym_node_security
WHERE node_id = '000';
DELETE FROM sym_node_identity
WHERE node_id = '000';
DELETE FROM sym_node_group
WHERE node_group_id = 'master';
DELETE FROM sym_node
WHERE node_id = '000';

INSERT INTO sym_node_group (node_group_id)
  VALUES ('master');
INSERT INTO sym_node_group (node_group_id)
  VALUES ('node');

INSERT INTO sym_node (node_id, node_group_id, external_id, sync_enabled, sync_url, schema_version, symmetric_version, database_type, database_version, heartbeat_time, timezone_offset, batch_to_send_count, batch_in_error_count, created_at_node_id)
  VALUES ('000', 'master', '000', 1, NULL, NULL, NULL, NULL, NULL, current_timestamp, NULL, 0, 0, '000');
INSERT INTO sym_node (node_id, node_group_id, external_id, sync_enabled, sync_url, schema_version, symmetric_version, database_type, database_version, heartbeat_time, timezone_offset, batch_to_send_count, batch_in_error_count, created_at_node_id)
  VALUES ('001', 'node', '001', 1, NULL, NULL, NULL, NULL, NULL, current_timestamp, NULL, 0, 0, '000');
INSERT INTO sym_node_identity
  VALUES ('000');
INSERT INTO sym_node_security (node_id, node_password, registration_enabled, registration_time, initial_load_enabled, initial_load_time, created_at_node_id)
  VALUES ('000', '5d1c92bbacbe2edb9e1ca5dbb0e481', 0, current_timestamp, 0, current_timestamp, '000');
INSERT INTO sym_node_security (node_id, node_password, registration_enabled, registration_time, initial_load_enabled, initial_load_time, created_at_node_id)
  VALUES ('001', '5d1c92bbacbe2edb9e1ca5dbb0e481', 1, NULL, 1, current_timestamp, '000');