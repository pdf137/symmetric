# Replicación de base de datos

## Requerimientos

- Java SE Runtime Environment 8

## Instalación

Descargar SymmetricDS desde https://sourceforge.net/projects/symmetricds/files/symmetricds/

Descomprimir

```bash
sudo unzip symmetric-server-*.zip -d /opt/
sudo mv /opt/symmetric-server-* /opt/symmetric-server
```

## Configuración

```bash
cd symmetric
sudo cp engines/* /opt/symmetric-server/engines/
sudo cp lib/* /opt/symmetric-server/lib/
```

Se crea las tablas de symmetric
```bash
sudo /opt/symmetric-server/bin/symadmin --engine master-000 create-sym-tables
```

```bash
cd sql
mysql -D confcyt -u root -p<nodos.sql
mysql -D confcyt -u root -p<info.sql
```

Habilitar registración:
```bash
sudo /opt/symmetric-server/bin/symadmin --engine master-000 open-registration node 001
sudo /opt/symmetric-server/bin/sym
```

Recargar esquema
```bash
sudo /opt/symmetric-server/bin/symadmin --engine master-000 reload-node 001
```

Instalar servicio en linux
```bash
sudo /opt/symmetric-server/bin/sym_service install
```